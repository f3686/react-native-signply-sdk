const path = require('path');

module.exports = {
  dependencies: {
    'react-native-signply-sdk': {
      root: path.join(__dirname, '..'),
    },
  },
};
