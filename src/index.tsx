import { NativeModules, Platform } from 'react-native';

const LINKING_ERROR =
  `The package 'react-native-signply-sdk' doesn't seem to be linked. Make sure: \n\n` +
  Platform.select({ ios: "- You have run 'pod install'\n", default: '' }) +
  '- You rebuilt the app after installing the package\n' +
  '- You are not using Expo managed workflow\n';

const SignplySdk = NativeModules.SignplySdk ? NativeModules.SignplySdk  : new Proxy(
      {},
      {
        get() {
          throw new Error(LINKING_ERROR);
        },
      }
    );

const SignplySDKParams = SignplySdk.SignplySDKParams;
console.log(SignplySDKParams.licenseB64);
