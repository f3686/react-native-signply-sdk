export class SignplySDKParams {
  constructor(
    licenseB64 = '',
    fileProperties = new SignplySDKFileProperties(),
    commonProperties = new SignplySDKCommonProperties(),
    widget = new SignplySDKWidget(),
    tsp = new SignplySDKTSP(),
    extra = new SignplySDKExtra(),
    certificate = new SignplySDKCertificate()
  ) {
    this.licenseB64 = licenseB64;
    this.fileProperties = fileProperties;
    this.commonProperties = commonProperties;
    this.widget = widget;
    this.tsp = tsp;
    this.extra = extra;
    this.certificate = certificate;
  }
}

export class SignplySDKFileProperties {
  constructor(
    documentPath = 'example.pdf',
    signedName = 'signply_document.pdf',
    isShareable = false,
    password = '',
    author = '',
    reason = '',
    contact = '',
    location = ''
  ) {
    this.documentPath = documentPath;
    this.signedName = signedName;
    this.isShareable = isShareable;
    this.password = password;
    this.author = author;
    this.reason = reason;
    this.contact = contact;
    this.location = location;
  }
}

export class SignplySDKCommonProperties {
  constructor(
    title = null,
    requestLocation = false,
    saveOnPrivateStorage = false,
    renderDefaultIndexPage = 0,
    reference = ''
  ) {
    this.title = title;
    this.requestLocation = requestLocation;
    this.saveOnPrivateStorage = saveOnPrivateStorage;
    this.renderDefaultIndexPage = renderDefaultIndexPage;
    this.reference = reference;
  }
}

export class SignplySDKWidget {
  constructor(
    widgetType = 'Manual',
    widgetFloatText = '',
    widgetFieldFieldName = '',
    widgetManualRatio = 2.5,
    widgetFixedPage = null,
    widgetFixedX = null,
    widgetFixedY = null,
    widgetFloatGapY = null,
    widgetFloatGapX = null,
    widgetCustomText = [],
    requestWidgetCustomText = false,
    widgetWidth = 150,
    widgetHeight = 75,
    signOnAllPages = false
  ) {
    this.widgetType = widgetType;
    this.widgetFloatText = widgetFloatText;
    this.widgetFieldFieldName = widgetFieldFieldName;
    this.widgetManualRatio = widgetManualRatio;
    this.widgetFixedPage = widgetFixedPage;
    this.widgetFixedX = widgetFixedX;
    this.widgetFixedY = widgetFixedY;
    this.widgetWidth = widgetWidth;
    this.widgetHeight = widgetHeight;
    this.widgetFloatGapY = widgetFloatGapY;
    this.widgetFloatGapX = widgetFloatGapX;
    this.widgetCustomText = widgetCustomText;
    this.requestWidgetCustomText = requestWidgetCustomText;
    this.signOnAllPages = signOnAllPages;
  }
}

export class SignplySDKTSP {
  constructor(
    tspActivate = false,
    tspURL = 'http://tsa.ecosignature.com:8779',
    tspUser = '',
    tspPassword = ''
  ) {
    this.tspActivate = tspActivate;
    this.tspURL = tspURL;
    this.tspUser = tspUser;
    this.tspPassword = tspPassword;
  }
}

export class SignplySDKExtra {
  constructor(
    autoOpen = false,
    viewLastPage = false,
    showReject = false,
    signatureColorHex = '#000000',
    signatureThickness = 10
  ) {
    this.autoOpen = autoOpen;
    this.viewLastPage = viewLastPage;
    this.showReject = showReject;
    this.signatureColorHex = signatureColorHex;
    this.signatureThickness = signatureThickness;
  }
}

export class SignplySDKCertificate {
  constructor(
    signCertP12B64 = null,
    signCertPassword = null,
    encKeyB64 = null,
    ltv = false
  ) {
    this.signCertP12B64 = signCertP12B64;
    this.signCertPassword = signCertPassword;
    this.encKeyB64 = encKeyB64;
    this.ltv = ltv;
  }
}
