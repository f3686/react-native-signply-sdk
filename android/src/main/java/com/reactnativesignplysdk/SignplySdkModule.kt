package com.reactnativesignplysdk

import android.app.Activity
import android.app.Activity.RESULT_FIRST_USER
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.util.Log
import androidx.core.content.FileProvider
import com.edatalia.signplysdk.data.*
import com.edatalia.signplysdk.presentation.SignplySDKLauncher
import com.facebook.react.bridge.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.EOFException
import java.io.File


class SignplySdkModule(reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext), ActivityEventListener {

    private val REQUEST_CODE_LAUNCH_SIGNPLY_SDK = 4555

    private lateinit var successCallback: Callback
    private lateinit var errorCallback: Callback

    init {
      reactContext.addActivityEventListener(this)
    }

    override fun getName(): String {
        return "SignplySdk"
    }


    @ReactMethod(isBlockingSynchronousMethod = true)
    fun launchSignply(signplySDKParamsJsonString: String, successCallback: Callback, errorCallback: Callback) {

      val signplySDKParams = Json.decodeFromString<SignplySDKParams>(signplySDKParamsJsonString)

      this.successCallback = successCallback
      this.errorCallback = errorCallback

      val folder = reactApplicationContext.getExternalFilesDir(null)
      val file = File(folder, signplySDKParams.fileProperties.documentPath)
      if (!file.exists()) {
        saveAssetsFileOnDestination(file)
      }

      val uri = FileProvider.getUriForFile(
        reactApplicationContext,
        reactApplicationContext.packageName + ".fileprovider",
        file
      )
      signplySDKParams.fileProperties.documentPath = uri.toString()

      val intent = Intent(reactApplicationContext, SignplySDKLauncher::class.java)
      intent.putExtra("signplySDKParams", signplySDKParams)
      Log.i("SignplySdkModule", "startActivityForResult")
      try {
        currentActivity?.startActivityForResult(intent, REQUEST_CODE_LAUNCH_SIGNPLY_SDK)
      } catch (e: Exception) {
        Log.e("SignplySdkModule", e.localizedMessage ?: "Unexpected error sdk")
      }
    }

  private fun saveAssetsFileOnDestination(file: File) {
    try {
      val inputStream = reactApplicationContext.assets.open("example.pdf").buffered()
      inputStream.use { input ->
        file.outputStream().use { output ->
          input.copyTo(output)
        }
      }
    } catch (e: Exception) {
      Log.e("SignplySdkModule", e.localizedMessage ?: "Unexpected error saving assets file")
    }
  }

  override fun onActivityResult(
    activity: Activity?,
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    Log.i("SignplySdkModule", "onActivityResult")
    if (requestCode == REQUEST_CODE_LAUNCH_SIGNPLY_SDK && resultCode == RESULT_OK && data != null && data.data != null) {
        successCallback.invoke(data.data.toString())
    } else if (requestCode == REQUEST_CODE_LAUNCH_SIGNPLY_SDK && resultCode == RESULT_FIRST_USER) {
        val extras = data?.extras ?: return
        var error = extras["signplySDKThrowable"] as Throwable?
        if (error == null) {
          error = Throwable("Unexpected error")
        }
        handleSigplySDKError(error)
      }
  }

  private fun handleSigplySDKError(signplySDKThrowable: Throwable) {
    val errorMessage: String = when (signplySDKThrowable) {
        is SignplySDKInvalidLicenseException -> {
          "La licencia no es válida"
        }
      is SignplySDKRenderPDFException -> {
        "No se ha podido renderizar el PDF"
      }
      is SignplySDKInvalidParamsException -> {
        "Parámetros incorrectos"
      }
      is SignplySDKSignPDFException -> {
        "No se ha podido realizar la firma"
      }
      is OutOfMemoryError -> {
        "El dispositivo no tiene suficiente memoria para procesar el documento"
      }
      is EOFException -> {
        "El dispositivo no tiene suficiente espacio para guardar el documento"
      }
      else -> {
        "Ha ocurrido un error inesperado"
      }
    }
    errorCallback.invoke(errorMessage)
  }

  override fun onNewIntent(intent: Intent?) {
    Log.i("SignplySdkModule", "onNewIntent")
  }

}
