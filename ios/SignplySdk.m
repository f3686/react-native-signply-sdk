#import <React/RCTBridgeModule.h>

@interface RCT_EXTERN_MODULE(SignplySdk, NSObject)

+ (BOOL)requiresMainQueueSetup
{
  return NO;
}

RCT_EXTERN_METHOD(launchSignply:(NSString *)signplySDKParamsString
                 withSuccessCallback:(RCTResponseSenderBlock)successCallback
                 withErrorCallback:(RCTResponseSenderBlock)errorCallback)



@end
