// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.9.2 (swiftlang-5.9.2.2.56 clang-1500.1.0.2.5)
// swift-module-flags: -target x86_64-apple-ios13.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name SignplySDK
// swift-module-flags-ignorable: -enable-bare-slash-regex
import Combine
import CommonCrypto
import CoreLocation
import CryptoKit
import DeveloperToolsSupport
import Foundation
import Network
import PDFKit
import PencilKit
import SecureBlackbox
import SecureBlackbox.SecureBlackboxCertificateManager
import SecureBlackbox.SecureBlackboxPDFSigner
@_exported import SignplySDK
import Swift
import SwiftUI
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public protocol SignatureViewDelegate : AnyObject {
  func swiftSignatureViewDidTapInside(_ view: SignplySDK.SignatureView)
  func swiftSignatureViewDidPanInside(_ view: SignplySDK.SignatureView)
  func signatureViewPointCapture(_ point: CoreFoundation.CGPoint, time: Swift.Int)
}
@objc @_inheritsConvenienceInitializers @_Concurrency.MainActor(unsafe) open class SignatureView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) weak open var delegate: (any SignplySDK.SignatureViewDelegate)?
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var maximumStrokeWidth: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var minimumStrokeWidth: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var strokeColor: UIKit.UIColor
  @objc @IBInspectable @_Concurrency.MainActor(unsafe) open var strokeAlpha: CoreFoundation.CGFloat {
    @objc get
    @objc set
  }
  @_Concurrency.MainActor(unsafe) open var signature: UIKit.UIImage? {
    get
  }
  @_Concurrency.MainActor(unsafe) open func clear()
  @_Concurrency.MainActor(unsafe) @objc required dynamic public init?(coder aDecoder: Foundation.NSCoder)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc override dynamic open func draw(_ rect: CoreFoundation.CGRect)
  @objc deinit
}
@_inheritsConvenienceInitializers @objcMembers @objc open class SignplySDKLauncher : ObjectiveC.NSObject {
  @objc public static func handleDocument(signplySDKParams: SignplySDK.SignplySDKParams, on controller: UIKit.UIViewController, successCallback: @escaping (_ url: Foundation.URL) -> (), rejectCallback: @escaping (_ reason: Swift.String) -> (), errorCallback: @escaping (_ error: Swift.String?) -> ())
  @objc override dynamic public init()
  @objc deinit
}
public enum SignplySdkLicenseResult {
  case valid
  case invalid
  case invalidB64
  case invalidIds
  case expired
  case internalError
  public static func == (a: SignplySDK.SignplySdkLicenseResult, b: SignplySDK.SignplySdkLicenseResult) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@_inheritsConvenienceInitializers @objcMembers @objc public class SignplySDKParams : ObjectiveC.NSObject, Swift.Decodable {
  @objc public var licenseB64: Swift.String
  @objc public var fileProperties: SignplySDK.SignplySDKFileProperties
  @objc public var commonProperties: SignplySDK.SignplySDKCommonProperties
  @objc public var widget: SignplySDK.SignplySDKWidget
  @objc public var tsp: SignplySDK.SignplySDKTSP
  @objc public var extra: SignplySDK.SignplySDKExtra
  @objc public var certificate: SignplySDK.SignplySDKCertificate
  @objc override dynamic public init()
  @objc public init(licenseB64: Swift.String, fileProperties: SignplySDK.SignplySDKFileProperties = SignplySDKFileProperties(), commonProperties: SignplySDK.SignplySDKCommonProperties = SignplySDKCommonProperties(), widget: SignplySDK.SignplySDKWidget = SignplySDKWidget(), tsp: SignplySDK.SignplySDKTSP? = nil, extra: SignplySDK.SignplySDKExtra? = nil, certificate: SignplySDK.SignplySDKCertificate? = nil)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKFileProperties : ObjectiveC.NSObject, Swift.Decodable {
  public var documentPath: Swift.String
  public var signedName: Swift.String
  public var isShareable: Swift.Bool
  public var password: Swift.String
  public var author: Swift.String
  public var reason: Swift.String
  public var contact: Swift.String
  public var location: Swift.String
  @objc override dynamic public init()
  @objc public init(documentPath: Swift.String, signedName: Swift.String? = nil, isShareable: Swift.Bool = false, password: Swift.String? = nil, author: Swift.String? = nil, reason: Swift.String? = nil, contact: Swift.String? = nil, location: Swift.String? = nil)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKCommonProperties : ObjectiveC.NSObject, Swift.Decodable {
  public var title: Swift.String?
  public var requestLocation: Swift.Bool
  public var renderDefaultIndexPage: Swift.Int?
  public var reference: Swift.String
  public var saveOnPrivateStorage: Swift.Bool
  @objc override dynamic public init()
  @objc public init(title: Swift.String? = nil, requestLocation: Swift.Bool, renderDefaultIndexPage: Swift.Int = 0, reference: Swift.String = "", saveOnPrivateStorage: Swift.Bool = false)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKWidget : ObjectiveC.NSObject, Swift.Decodable {
  public var widgetType: SignplySDK.SignplySDKWidgetType
  public var widgetFloatText: Swift.String?
  public var widgetFieldFieldname: Swift.String?
  public var widgetManualRatio: Swift.Float
  public var widgetFixedPage: Swift.Int?
  public var widgetFixedX: Swift.Int?
  public var widgetFixedY: Swift.Int?
  public var widgetFloatGapY: Swift.Int?
  public var widgetFloatGapX: Swift.Int?
  public var widgetCustomText: [SignplySDK.SignplySDKWidgetCustomText]
  public var widgetCustomTextB64: Swift.String?
  public var requestWidgetCustomText: Swift.Bool
  public var widgetWidth: Swift.Int
  public var widgetHeight: Swift.Int
  public var flexible: Swift.Bool?
  public var signOnAllPages: Swift.Bool
  @objc override dynamic public init()
  @objc public init(widgetType: SignplySDK.SignplySDKWidgetType, widgetFloatText: Swift.String? = nil, widgetFieldFieldname: Swift.String? = nil, widgetManualRatio: Foundation.NSNumber? = nil, widgetFixedPage: Foundation.NSNumber? = nil, widgetFixedX: Foundation.NSNumber? = nil, widgetFixedY: Foundation.NSNumber? = nil, widgetWidth: Foundation.NSNumber? = nil, widgetHeight: Foundation.NSNumber? = nil, widgetFloatGapX: Foundation.NSNumber? = nil, widgetFloatGapY: Foundation.NSNumber? = nil, widgetCustomText: [SignplySDK.SignplySDKWidgetCustomText] = [], widgetCustomTextB64: Swift.String? = nil, requestWidgetCustomText: Foundation.NSNumber? = nil, flexible: Foundation.NSNumber? = false, signOnAllPages: Swift.Bool = false)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKTSP : ObjectiveC.NSObject, Swift.Decodable {
  public var tspActivate: Swift.Bool
  public var tspURL: Swift.String
  public var tspUser: Swift.String?
  public var tspPassword: Swift.String?
  @objc override dynamic public init()
  @objc public init(tspActivate: Foundation.NSNumber? = nil, tspURL: Swift.String? = nil, tspUser: Swift.String? = nil, tspPassword: Swift.String? = nil)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKExtra : ObjectiveC.NSObject, Swift.Decodable {
  public var autoOpen: Swift.Bool
  public var viewLastPage: Swift.Bool
  @objc public var showReject: Swift.Bool
  public var signatureColorHex: Swift.String
  public var signatureThickness: Swift.Int
  public var fullScreen: Swift.Bool?
  @objc override dynamic public init()
  @objc public init(autoOpen: Foundation.NSNumber? = false, viewLastPage: Foundation.NSNumber? = nil, signatureColorHex: Swift.String? = nil, signatureThickness: Swift.Int = 10, showReject: Swift.Bool = false, fullScreen: Foundation.NSNumber? = true)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKCertificate : ObjectiveC.NSObject, Swift.Decodable {
  public var signCertP12B64: Swift.String?
  public var signCertPassword: Swift.String?
  public var encKeyB64: Swift.String?
  public var ltv: Swift.Bool
  @objc override dynamic public init()
  @objc public init(signCertP12B64: Swift.String? = nil, signCertPassword: Swift.String? = nil, encKeyB64: Swift.String? = nil, ltv: Swift.Bool = false)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class SignplySDKWidgetCustomText : ObjectiveC.NSObject, Swift.Decodable {
  public var fontSize: Swift.Int
  public var text: Swift.String
  @objc override dynamic public init()
  @objc public init(fontSize: Swift.Int, text: Swift.String)
  @objc deinit
  required public init(from decoder: any Swift.Decoder) throws
}
@objc public enum SignplySDKWidgetType : Swift.Int, Swift.Decodable, Swift.CaseIterable, Swift.Identifiable, Swift.CustomStringConvertible {
  case Manual
  case Field
  case Fixed
  case Float
  public init(from decoder: any Swift.Decoder) throws
  public var id: SignplySDK.SignplySDKWidgetType {
    get
  }
  public var description: Swift.String {
    get
  }
  public init?(rawValue: Swift.Int)
  public typealias AllCases = [SignplySDK.SignplySDKWidgetType]
  public typealias ID = SignplySDK.SignplySDKWidgetType
  public typealias RawValue = Swift.Int
  public static var allCases: [SignplySDK.SignplySDKWidgetType] {
    get
  }
  public var rawValue: Swift.Int {
    get
  }
}
extension SignplySDK.SignplySdkLicenseResult : Swift.Equatable {}
extension SignplySDK.SignplySdkLicenseResult : Swift.Hashable {}
extension SignplySDK.SignplySDKWidgetType : Swift.Equatable {}
extension SignplySDK.SignplySDKWidgetType : Swift.Hashable {}
extension SignplySDK.SignplySDKWidgetType : Swift.RawRepresentable {}
