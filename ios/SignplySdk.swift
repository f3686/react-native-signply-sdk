import UIKit
import SignplySDK

@objc(SignplySdk)
class SignplySdk: NSObject {
    
    @objc(launchSignply:withSuccessCallback:withErrorCallback:)
    func launchSignply(signplySDKParamsString: String, successCallback: @escaping RCTResponseSenderBlock, errorCallback: @escaping RCTResponseSenderBlock) -> Void {
        
         
        do {
            let signplySDKParamsJSONData = Data(signplySDKParamsString.utf8)
            let signplySDKParams = try JSONDecoder().decode(SignplySDKParams.self, from: signplySDKParamsJSONData)
            
            let folder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            let file = folder.appendingPathComponent( signplySDKParams.fileProperties.documentPath )
            if !FileManager.default.fileExists(atPath: file.path) {
                let sourceURL = Bundle.main.url(forResource: "example", withExtension: "pdf")!
                
                do { try FileManager.default.copyItem(at: sourceURL, to: file) } catch {}
            }
            signplySDKParams.fileProperties = SignplySDKFileProperties(documentPath: file.path)
            
            print(signplySDKParams.fileProperties)
            
            DispatchQueue.main.async {
              SignplySDKLauncher.handleDocument(signplySDKParams: signplySDKParams, on:  RCTPresentedViewController()!, successCallback: { url in
                  successCallback([url.absoluteString])
                  }, rejectCallback: { reason in }, errorCallback: {error in
                    errorCallback([error ?? "Unexpected error"])
                  })
            }
        } catch {
            print(error)
            errorCallback([error.localizedDescription])
        }
    }
}
